# Markdown与Pandoc教程

## 简介
**Markdown**是一种轻量级、文本格式**标记语言**，具有轻量化、易读易写的优点，支持图片、图表及数学公式等，许多网站内置支持Markdown格式显示，如Github、reddit、简书等。**Markdown**可以通过转换工具，如**Pandoc**， 转换为多种其他文档格式，如Word、PDF、HTML等。

本文简要介绍**Markdown**及**Pandoc**使用方法。

## Markdown

### 基本语法

本节介绍**Markdown**常用基本语法。

#### 标题

在想要设置为标题的文字前面加#来表示。

一个#是一级标题，二个#是二级标题，以此类推。支持六级标题。

示例：

``` markdown
# 这是一级标题
## 这是二级标题
### 这是三级标题
#### 这是四级标题
##### 这是五级标题
###### 这是六级标题
```

#### 字体

- 加粗
  
要加粗的文字左右分别用两个*号包起来

- 斜体

要倾斜的文字左右分别用一个*号包起来

- 斜体加粗

要倾斜和加粗的文字左右分别用三个*号包起来

- 删除线

要加删除线的文字左右分别用两个~~号包起来

示例：

``` markdown
**这是加粗的文字**
*这是倾斜的文字*
***这是斜体加粗的文字***
~~这是加删除线的文字~~
```

#### 列表

无序列表用 ` - + *` 任何一种都可以

示例：

``` markdown
- 列表内容
+ 列表内容
* 列表内容

注意：- + * 跟内容之间都要有一个空格
```

有序列表用 `数字加点` 形式

示例：

``` markdown
1. 列表内容
2. 列表内容
3. 列表内容

注意：序号和内容之间都要有一个空格
```

#### 图片

语法：

``` markdown
![图片alt](图片地址 ''图片title'')

图片alt就是显示在图片下面的文字，相当于对图片内容的解释。
图片title是图片的标题，当鼠标移到图片上时显示的内容。title可加可不加
```

#### 表格

语法：

``` markdown
表头|表头|表头
---|:--:|---:
内容|内容|内容
内容|内容|内容

第二行分割表头和内容。
- 有一个就行，为了对齐，多加了几个
文字默认居左
-两边加：表示文字居中
-右边加：表示文字居右
注：原生的语法两边都要用 | 包起来。此处省略
```

示例：

``` markdown
姓名|技能|排行
--|:--:|--:
刘备|哭|大哥
关羽|打|二哥
张飞|骂|三弟
```

##### 公式

**Markdown**支持 $Latex$格式数学公式，包括两种：

- 行内公式（内嵌于正文文本中间，与正文文字行高相等）： `$数学公式$`
- 行间公式（单独成行）： `$$数学公式$$`

如：

``` markdown
$$
函数$ L(Y,f(X))=(Y-f(X))^2 $
函数
$$ 
L(Y,f(X))=(Y-f(X))^2 
$$
```

行间公式可以添加编号，使用 `\tag{...}` 标签，如

``` markdown
$$
\begin{split}
a &= b \\
c &= d \\
e &= f 
\end{split}\tag{1.3}
$$
```

$$
\sum_{i=0}^{n}i^2 + \left.\frac{1}{2}x^2\right|_0^1
$$
$$
E=mc^2
$$

更详细的公式编辑可参考更多资源：

- [编辑你的数学公式—markdown中latex的使用](https://www.cnblogs.com/csu-lmw/p/10434854.html)
- [Markdown中Latex数学公式命令](https://www.jianshu.com/p/0ea47ae02262)
- [Markdown下LaTeX公式、编号、对齐](https://www.zybuluo.com/fyywy520/note/82980)

#### 链接

语法：

``` markdown
[超链接名](超链接地址 "超链接title")

title可加可不加
```

示例：

``` markdown
[百度](http://baidu.com)
```

### Markdown编辑器

普通文本编辑器即可进行Markdown文档编辑。这里推荐[Visual Studio Code](https://code.visualstudio.com/Download) 编辑器。使用该编辑器进行文档编写，编写后将文件命名为“文件名.md”的形式。（该编辑器可以实时预览显示效果，请参考[VS Code 实时预览方法](https://www.cnblogs.com/shawWey/p/8931697.html)）。

#### VS Code中Markdown数学公式实时预览
在Visual Studio Code中安装“[Markdown+Math](https://www.npmjs.com/package/mdmath)”插件，即可实现数学公式实时预览，具体步骤如下：

a.打开Visual Studio Code，在键盘上按“F1”键。

b.在弹出的输入框中输入：**extensions**，从中选择**Extensions: Install Extension**。

![图](./pandoc_demos/数学公式实时预览步骤b图.png)

c.在左侧搜索框中搜索**Markdown+Math**,找到对应插件点击**Install**即可。

d.将Markdown+math插件的状态设置为**Enable**即完成全部设置。

![图](./pandoc_demos/markdownmath设置为Enable.png)

e.在Visual Studio Code中实时预览Markdown格式文件时，即可同时实时预览公式。

![图](./pandoc_demos/数学公式实时预览示意图.png)

## Pandoc

Pandoc是**标记语言**转换工具，可实现不同标记语言间的格式转换，堪称该领域中的“瑞士军刀”。Pandoc以**命令行**形式实现与用户的交互，可支持多种操作系统。

Pandoc支持的标记语言格式如下：

Pandoc可读取的原格式：markdown、docx、rst、html、commonmark、docbook、epub、haddock、json、latex、markdown_github、markdown_mmd、markdown_phpextra、markdown_strict、mediawiki、native、odt、opml、org、t2t、textile、twiki 。

Pandoc可生成的目标格式：  markdown、docx、rst、html、asciidoc、beamer、commonmark、context、docbook、docbook5、dokuwiki、dzslides、epub、epub3、fb2、haddock、html5、icml、json、latex、man、markdown_github、markdown_mmd、markdown_phpextra、markdown_strict、mediawiki、native、odt、opendocument、opml、org、plain、revealjs、rtf、s5、slideous、slidy、tei、texinfo、textile、zimwiki。

### 安装
- Windows平台
下载[安装包](https://pandoc.org/installing.html#windows)并执行安装程序

- Linux平台
Pandoc 已经被集成到系统的软件源内，可直接从软件源安装：

```
sudo apt-get install pandoc
```

### 使用方法
- Windows平台
打开**cmd**，通过**cd**指令进入pandoc安装目录下，并将要转换的文件放在这个目录下。然后在命令行中输入pandoc命令（见下）。

- Linux平台
在要转换的文件目录下打开终端窗口，输入pandoc命令(见下)。

Pandoc官方命令手册见 [Pandoc User's Guide](https://pandoc.org/MANUAL.html#option--standalone)

常用命令如下：

命令均为：“pandoc + 参数 ”的形式
```
pandoc -o output.html input.txt

-o: 输出到文件output.html
```
上式命令为：将输入文件input.txt转换生成为output.html，原输入文件会保留

```
pandoc -f markdown -t html  -o hello.html  hello.txt

-f: 指定输入格式(from)，比如docx、epub、md、html等

-t: 指定输出格式(to)，比如docx、epub、md、html等
```
上式命令为：将hello.txt以markdown格式输入，转换为html格式

还有其他参数如下：
```
-N: 为标题自动添加编号

--toc: 添加目录

-s: 生成有页眉和页脚的独立文件

-v: 打印版本信息

-h：显示语法帮助

--verbost: 显示详细调试信息

--log：指定输出日志信息

--list-input-formats：列出支持的输入格式

--list-output-formats：列出支持的输出格式

--list-extensions：列表支持Markdown扩展，后面跟一个+或者-说明是否在pandoc的Markdown中默认启用

--list-highlight-languages:列出语法突出显示支持的语言

--list-highlight-styles:列出支持语法高亮的样式。
```

### 使用示例
**示例一**：现有一个“test.md”的文档，将其转换为***html***的格式。

步骤：

1.进入文档所在文件夹，在空白处右键点击“在终端打开”，呼出终端。

![图1](./pandoc_demos/在md文档所在处呼出终端.png)
2.在终端中输入命令：pandoc -f markdown -t html -o test.html test.md ，即可生成“test.html”。

![图2](./pandoc_demos/输入命令生成html.png)

转换前后效果如下：

![图3](./pandoc_demos/示例一转换前后对比.png)


**示例二**：现有一个“test.docx”的文档，将其转换为***rst***的格式。

步骤：

1.进入文档所在文件夹，在空白处右键点击“在终端打开”，呼出终端。

![图4](./pandoc_demos/在docx文档所在处呼出终端.png)
2.在终端中输入命令：pandoc -o test.rst test.docx ，即可生成“test.rst”。(对比示例一，可以不使用“-f”和“-t”参数，这时Pandoc会根据输入/输出文件自动判断类型)。

![图5](./pandoc_demos/输入命令生成rst.png)

转换前后效果如下：

![图6](./pandoc_demos/示例二转换前后对比.png)

**示例三**：现有三个“rst”格式的文档，需要将其合成一个有目录的“html”文档，并为标题自动添加编号。

![图7](./pandoc_demos/示例三转换前图.png)

步骤：

1.进入文档所在文件夹，在空白处右键点击“在终端打开”，呼出终端。

2.在终端中输入命令：pandoc -s -N --toc -o test.html test1.rst test2.rst test3.rst  ，即可生成有目录的“test.html”，且文件中已对标题自动编号。若选择生成docs，则生成的文件中不会对标题自动编号，但是各级标题都已划分为docx文件的“样式”，通过后续套用样式模板的方法，即可自动编号。

![图8](./pandoc_demos/示例三转换后图.png)
