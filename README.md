# 文档_示例

演示Gitlab协作（项目及文档管理）

## Markdown与Pandoc教程

[Markdown与Pandoc教程](./markdown_and_pandoc.md)

### 示例：使用Pandoc转换Markdown格式


#### 转换为html

``` bash
pandoc .\markdown_and_pandoc.md -o markdown.html
```

#### 转换为word（参照已有模板）

``` bash
pandoc .\markdown_and_pandoc.md -f markdown --reference-doc=template.docx --toc -o markdown.docx
```
